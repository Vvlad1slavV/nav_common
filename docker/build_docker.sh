#!/usr/bin/env bash

ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
EXEC_PATH=$PWD
CUDA_VERSION="11.7.1"
NVIDIA_DIST="nvidia/cuda:${CUDA_VERSION}-base-ubuntu20.04"
BASE_DIST="ubuntu:focal"
JETSON_DIST="ubuntu:bionic"


echo "Start building in: $ROOT_DIR"

cd $ROOT_DIR

if [[ $1 = "--nvidia" ]] || [[ $1 = "-n" ]]
  then
  docker build -t nav-img -f $ROOT_DIR/docker/Dockerfile $ROOT_DIR \
                                  --network=host \
                                  --build-arg BASE_IMG=$NVIDIA_DIST \
                                  --build-arg ROS1_DISTRO="noetic"
elif [[ $1 = "--jetson" ]] || [[ $1 = "-j" ]]
  then
  echo "Ubuntu 18.04"
  docker build -t nav-img -f $ROOT_DIR/docker/Dockerfile $ROOT_DIR \
                                  --network=host \
                                  --build-arg BASE_IMG=$JETSON_DIST \
                                  --build-arg ROS1_DISTRO="melodic"
else
    echo "[!] If you use nvidia gpu, please rebuild with -n or --nvidia argument"
    docker build -t nav-img -f $ROOT_DIR/docker/Dockerfile $ROOT_DIR \
                                  --network=host \
                                  --build-arg BASE_IMG=$BASE_DIST \
                                  --build-arg ROS1_DISTRO="noetic"
fi

cd $EXEC_PATH
